<?php

namespace App\Controller;

use App\Repository\ScoreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ApiController extends AbstractController
{
    
    //=========================//
    //   route de base
    //========================// 

    /**
    * @Route("/", name="api")
    */
    public function index(): Response
    {
        return $this->render('api/index.html.twig', [
            'controller_name' => 'ApiController',
        ]);
    }

    //=========================//
    //   ENVOI api méthode GET
    //========================// 

    /**
     * @Route("/api/score/", name="api_score_index", methods={"GET"})
     */
    public function ApiScore(ScoreRepository $scoreRepository): Response
    {

        return $this->json($scoreRepository->findAll(), 200, [], ['groups' => 'score:read']);
    }


     //=========================//
    //   ENVOI api méthode GET
    //   10 meilleurs resultats
    //========================// 

    /**
     * @Route("/api/score/best", name="api_score_best", methods={"GET"})
     */
    public function ApiScoreBest(ScoreRepository $scoreRepository): Response
    {

        return $this->json($scoreRepository->findByExampleField(), 200, [], ['groups' => 'score:read']);
    }


    //==========================================//
    //   Recevoir données pour api méthode POST
    //=========================================//

    /**
     *  @Route("/api/score/add", name="api_score_add", methods={"POST"})
     */
    public function add(Request $request, SerializerInterface $serializer, EntityManagerInterface $em)
    {
        //envoi requête recevoir (ici json) (lire contenu requêt http)
        $jsonRecu = $request->getContent();
        
        //essai de désérialiser l'objet sinon envoi message via catch
    
            //désérialisation transforme json en objet (complexe)
            $scoresRecus = $serializer->deserialize($jsonRecu, Score::class, 'json');
            dump($scoresRecus);

            //persister l'objet puis envoi données(requête sql) vers bdd
            $em->persist($scoresRecus);
            $em->flush($scoresRecus);

            //réponse en json et statut 201 (ressource créer sur le serveur ok et nlle ressource créée)
            return $this->json($scoresRecus, 201, [], ['groups' => 'score:read']);
    
    }
}

