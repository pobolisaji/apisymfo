<?php

namespace App\DataFixtures;

use App\Entity\Score;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

        for ($i = 0; $i < 20; $i++) {
            $score = new Score();
            $score->setScoreGame(mt_rand(10, 100));
            $score->setPseudoGame('score '.$i);
            $manager->persist($score);
        }
        $manager->flush();
    }
}
