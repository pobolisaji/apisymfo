<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ScoreRepository;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ScoreRepository::class)
 */
class Score
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("score:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("score:read")
     */
    private $pseudoGame;

    /**
     * @ORM\Column(type="integer")
     * @Groups("score:read")
     */
    private $scoreGame;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPseudoGame(): ?string
    {
        return $this->pseudoGame;
    }

    public function setPseudoGame(string $pseudoGame): self
    {
        $this->pseudoGame = $pseudoGame;

        return $this;
    }

    public function getScoreGame(): ?int
    {
        return $this->scoreGame;
    }

    public function setScoreGame(int $scoreGame): self
    {
        $this->scoreGame = $scoreGame;

        return $this;
    }
}
